# Peekskill_GIS

Peekskill GIS data from publicly available sources put in a QGIS package for easy downloading and viewing. Many of the layers are added as Web Feature Service layers so that they will be continually updated.